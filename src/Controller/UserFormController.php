<?php
/**
 * Created by PhpStorm.
 * User: Roma
 * Date: 22/04/2018
 * Time: 17:16
 */

namespace App\Controller;


use App\Form\UserFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserFormController extends Controller
{

    /**
     * @Route("/", name="home")
     */
    public function formAction(Request $request)
    {
      $form = $this->createForm(UserFormType::class);

      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
          return $this->render('user_info.html.twig', [
            'name' => $form->get('name')->getData(),
            'lastName' => $form->get('lastName')->getData(),
            'info' => $form->get('info')->getData(),
            'sex' => $form->get('sex')->getData(),
            'dateOfBirth' => $form->get('dateOfBirth')->getData()
          ]);
      }

      return $this->render('userForm.html.twig', [
          'userForm' => $form->createView()
      ]);

    }
}